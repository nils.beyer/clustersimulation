#!/usr/users/nbeyer1/.conda/envs/d3t/bin/python

import numpy as np
import pandas as pd
from d3t import statistics as st
import json

import glob

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('folder', type=str)
parser.add_argument('N', type=int)
parser.add_argument('seats', type=int)
parser.add_argument('-invehicledelay', type=float)
parser.add_argument('-totaldelay', type=float)

folder = parser.parse_args().folder
NTRANSPORTERS = parser.parse_args().N
seats = parser.parse_args().seats
in_vehicle_delay = parser.parse_args().invehicledelay
total_delay = parser.parse_args().totaldelay

path = '/usr/users/nbeyer1/clustersimulation/data/'+ folder
if in_vehicle_delay:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats) + '/' +str(in_vehicle_delay)
elif total_delay:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats) + '/' +str(total_delay)
else:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats)

if not in_vehicle_delay:
    in_vehicle_delay = 2.0
if not total_delay:
    total_delay = 6.0

with open(subpath+'/seeddict.json', 'r') as infile:
     seed_dict = json.load(infile)

if (seats != seed_dict['seats'] or NTRANSPORTERS != seed_dict['transporters']):
    print("Parameters don't match!")
norm = seed_dict['norm']

tmax_raw = seed_dict['tmax']


print('seats = '+str(seats) + ' Transporters = ' + str(NTRANSPORTERS) + ' tmax = ' +str(tmax_raw))


#for liste in result_dict.values():
#    liste = []
wait_list = []
absolute_wait_list = []
travel_list = []
rate_list = []
occ_list = []
profit_list = []
t_list = []
idle_list = []
rejection_list = []
total_requests_list = []
public_good_list = []
wait_histo_list = []
#remaining_rate = seed_dict['rates'] #why did i call it this way?
remaining_rate = [float((frame.split('/')[-1]).split('_')[0]) for frame in glob.glob(str(subpath)+'/*jobs*')]
for normalized_rate in remaining_rate:
    print('Processing: rate = ' + str(normalized_rate))
    rate = normalized_rate*(float(NTRANSPORTERS)/(2*norm))
    tmax = tmax_raw/rate
    raw_loads = pd.read_csv(subpath+'/'+str(normalized_rate)+"_raw_loads.csv", sep=',', header='infer', index_col = 0)
    raw_transporters = pd.read_csv(subpath+'/'+str(normalized_rate)+"_raw_transporters.csv", index_col = 0)
    raw_jobs = pd.read_csv(subpath+'/'+str(normalized_rate)+"_raw_jobs.csv", index_col = 0) #thos got screwed up with the wrong index
    raw_idleperiods = pd.read_csv(subpath+'/'+str(normalized_rate)+"_raw_idleperiods.csv", index_col = 0)
    
    #idle time
    #raw_idleperiods = raw_idleperiods.fillna(tmax) #faulty data with nan in there
    
    idletime = ((raw_idleperiods.loc[raw_idleperiods['end epoch'] > 0])['end epoch'].sum(axis=0) - (raw_idleperiods.loc[raw_idleperiods['end epoch'] > 0])['start epoch'].sum(axis=0))/float(NTRANSPORTERS)/tmax #throw out nan's completely

    #service time
    
    
    #occupancy
    
    jobs = st.compute_jobs(raw_jobs, raw_loads, raw_transporters, tmax)
    
    transporter_cargo_sizes = st.compute_transporter_cargo_sizes(jobs,raw_transporters, tmax)
    
    cargos = transporter_cargo_sizes[['transporter','cargo size', 'period']]
    
    scargos = cargos.groupby(['transporter','cargo size']).agg(np.nansum).reset_index()
    
    avge_occupancy = ((scargos['cargo size']*scargos['period']).groupby(scargos['transporter']).sum()/(scargos['period']).groupby(scargos['transporter']).sum()).mean()
    
    fraction_shared = (scargos.loc[scargos['cargo size'] > 1])['period'].sum()/(scargos['period']).sum() #2 pals or more in one bus

    loads = st.compute_loads(raw_loads, raw_jobs)#['origin', 'destination', 'direct travel time', 'request epoch', 'reject epoch', 'transporter', 'pick-up epoch', 'delivery epoch', 'sojourn time', 'queueing time', 'lead time', 'waiting time', 'travel time', 'service time', 'inter-arrival time', 'relative sojourn time', 'relative waiting time', 'relative travel time', 'arrival number', 'system size at arrival epoch']
    waiting_time = loads['relative waiting time'].mean()#/norm
    absolute_waiting_time = loads['waiting time'].mean()/norm
    travel_time = loads['relative travel time'].mean()
    #public good    
    public_good = (loads.loc[loads['assign epoch'] > 0])['direct travel time'].sum()/(NTRANSPORTERS*tmax*(1 - idletime))
    wait_histo = []
    wait_histo.append(list(np.histogram(loads['waiting time'].dropna())[0]))
    wait_histo.append(list(np.histogram(loads['waiting time'].dropna())[1]/norm))    
    wait_histo_list.append(wait_histo)
    
    loads['inverse_travel'] = loads['relative travel time'].apply(func = (lambda x: 1/x if x > 0 else -1))
    profit = ((loads[['inverse_travel', 'direct travel time']].product(axis=1)).sum())/norm/NTRANSPORTERS/tmax#[0:int(-2*normalized_rate*NTRANSPORTERS)]
    print('profit = '+str(profit))
    
    
    #amount_delivered = np.count_nonzero(np.isfinite(np.asarray(raw_loads['transporter'])))
    number_of_rejections = raw_loads['reject epoch'].count()
    total_requests = len(raw_loads.index)
    rejection_ratio = float(number_of_rejections)/total_requests 
    
    travel_list.append(travel_time)
    wait_list.append(waiting_time)
    absolute_wait_list.append(absolute_waiting_time)
    rate_list.append(normalized_rate)
    occ_list.append(avge_occupancy)
    profit_list.append(profit)
    t_list.append(tmax)
    idle_list.append(idletime)
    rejection_list.append(rejection_ratio)
    total_requests_list.append(total_requests)
    public_good_list.append(public_good)
    wait_histo_list.append(wait_histo)

#'leadtime':completiontime['completion_time'].mean(axis=0),
#, qsteps
result_dict = {'rate':rate_list, 'relative_waiting_time':wait_list, 'absolute_waiting_time': absolute_wait_list, 'travel_time':travel_list, 'occupancy':occ_list, 'profit':profit_list, 'runtime':t_list, 'idletime':idle_list, 'rejection_ratio':rejection_list, 'requests': total_requests_list, 'wait_histograms':wait_histo_list, 'public_good': public_good_list}

with open(subpath+'/resultdict.json', 'w') as outfile:
    json.dump(result_dict, outfile)
print('result saved: N= '+str(NTRANSPORTERS)+' s: '+str(seats))
