#!/usr/users/nbeyer1/.conda/envs/d3t_3/bin/python

# This is a script to perform a single d3t simulation on the gwdg cluster.


import numpy as np
np.random.seed(1)
import random
random.seed(1)
import json
import networkx as nx
import pandas as pd
import pickle

from d3t.d3tsystem import D3TSystem
from d3t.space import Network
from d3t.space import R2Euclidian
from d3t.observers import RawDataObserver
from rideshared3t.eepa_dispatcher import EEPADispatcherModel as Ellipse #needs module 'tkinter'

import load_generators as lg
import argparse
from collections import defaultdict
#Parse the Commandline Arguments

parser = argparse.ArgumentParser()

parser.add_argument('rate', type=float)
parser.add_argument('folder', type=str)
parser.add_argument('N', type=int)
parser.add_argument('seats', type=int)
parser.add_argument('topology', type=str)
parser.add_argument('-invehicledelay', type=float)
parser.add_argument('-totaldelay', type=float)

normalized_rate = parser.parse_args().rate
folder = parser.parse_args().folder
NTRANSPORTERS = parser.parse_args().N
seats = parser.parse_args().seats
topology = parser.parse_args().topology
in_vehicle_delay = parser.parse_args().invehicledelay
total_delay = parser.parse_args().totaldelay

path = '/usr/users/nbeyer1/clustersimulation/data/'+ folder

#setting defaults for optional arguments
if in_vehicle_delay:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats) + '/' +str(in_vehicle_delay)
elif total_delay:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats) + '/' +str(total_delay)
else:
    subpath = path + '/' + str(NTRANSPORTERS) + '/' + str(seats)

if not in_vehicle_delay:
    in_vehicle_delay = 2.0
if not total_delay:
    total_delay = 6.0



#print('received: rate= '+str(normalized_rate) + ' folder: '+str(folder), NTRANSPORTERS, seats, topology, in_vehicle_delay, total_delay)


#Open the dictionary containing the parameters in the folder given to this skript
with open(subpath+'/'+'seeddict.json', 'r') as infile:
    seed_dict = json.load(infile)
    
    
norm = seed_dict['norm']
number_of_requests = seed_dict['tmax']


#normalized rate
rate = normalized_rate*(float(NTRANSPORTERS)/(2*norm))
tmax = number_of_requests/rate #to keep amount of data constant


#define space, load generator and starting positions based on topology
if topology == 'euclidian_plane':
    tspace = R2Euclidian()
    generator = lg.load_generator_r2_uniform(rate)
    starting_positions = [np.random.uniform(0.0, 1.0, size = 2) for i in range(NTRANSPORTERS)]
else:
    G = nx.read_gpickle(f'{path}/{topology}.gpickle')
    if topology.split('_')[0] == 'manhattan' or topology == 'goettingen':
        taxi_speed = 7.1*1609.3/3600
        factory = lambda: defaultdict(lambda: float('inf'))
        clean_distance =pickle.load(open(f'/usr/users/nbeyer1/clustersimulation/data/Network_{topology.split("_")[0]}_distances.p', 'rb'))
        #bugged, but also not needed anymore, taken care of at graph creation
#        for item in clean_distance.items():
#            clean_distance[item[0]] = item[1]/taxi_speed
        dirty_dict = {node:defaultdict(factory,neighbors) for node,neighbors in clean_distance.items()}  # used to be defaultdict(factory,**neighbors) what changed?
        predecessors =pickle.load(open(f'/usr/users/nbeyer1/clustersimulation/data/Network_{topology.split("_")[0]}_predecessors.p', 'rb'))
        tspace = Network(graph = G, predecessors = predecessors, distances = dirty_dict)
    else: 
        tspace = Network(G)
    if topology == 'manhattan_data':
        types = {"start id": np.int64, "dest id": np.int64, "seconds to next": np.float64}
        manhattan_day = pd.read_csv(str(subpath)+'/'+str(normalized_rate)+'.csv', sep=' ', header=None, names=("start id","dest id","seconds to next"), dtype=types)
        generator = lg.load_generator_from_data(manhattan_day)
    elif(folder == 'correlation/center'):
        print('folder: '+str(folder))
        generator = lg.load_generator_network_center(rate, G)
    else:
        generator = lg.load_generator_network_uniform(rate, G)
    starting_positions = [random.sample(G.nodes(), 1)[0] for i in range(NTRANSPORTERS)]




# Define the D3T System

d3ts = D3TSystem(space=tspace,
		load_generators=[generator],
		transporter_num=NTRANSPORTERS,
		transporter_positions=starting_positions,
		dispatcher_model = Ellipse,
		observer_models=[(RawDataObserver, {'space': tspace}), ],
		dispatcher_initialization=dict(space=tspace,  cost='distance', ellipse_ecc_inv=18,## this value must always be >=1. If this is 1, the ellipse has zero area. >= 2 get weird as secondary ellipses can keep growing
                               transporter_capacities=seats,
                               # parameters for the future request ellipsis handler
                               split_at_slack = 0, #break time? -> set to 0 to prevent bug
                               relative_in_vehicle_time_max = in_vehicle_delay, # must deliver no later than this times a car could
                               relative_delivery_delay_max = total_delay, # This is the customer "patience" parameter. No request will be delivered later than requested_pickup_time + relative_delivery_delay_max * private_car_travel_time
                               # Requests are rejected if this constraint cannot be fulfilled
                               dropoff_max_delay_always_atleast = 0, # customers must tolerate at least x seconds delay for dropoff
                               pickup_promised_window = norm)# we promise our pickup will be in a window of length 20 mins               
		)
d3ts.execute_until(tmax)

rawdata_obs = d3ts._observers[0]

# Computing statistics

raw_loads = rawdata_obs.get_raw_loads_df()
raw_transporters = rawdata_obs.get_raw_transporters_df()
raw_jobs = rawdata_obs.get_raw_jobs_df()
raw_idleperiods = rawdata_obs.get_raw_idleperiods_df()
#idleperiods are bugged for those 2 first topologies
if topology.split('_')[0] == 'manhattan' or topology == 'euclidian_plane'or topology == 'goettingen':
    raw_idleperiods = pd.DataFrame(data=rawdata_obs._idleperiods, columns = rawdata_obs.IDLEPERIOD_COLUMNS)


raw_loads.to_csv(subpath+'/'+str(normalized_rate)+"_raw_loads.csv")
raw_transporters.to_csv(subpath+'/'+str(normalized_rate)+"_raw_transporters.csv")
raw_jobs.to_csv(subpath+'/'+str(normalized_rate)+"_raw_jobs.csv")
raw_idleperiods.to_csv(subpath+'/'+str(normalized_rate)+"_raw_idleperiods.csv")

with open(subpath+'/'+'seeddict.json', 'r') as infile:
    seed_dict = json.load(infile)
    
ratelist = seed_dict['rates']
ratelist.append(normalized_rate)
seed_dict.update({'rates':ratelist})

with open(subpath+'/'+'seeddict.json', 'w') as outfile:
    json.dump(seed_dict, outfile)
    				
#import json
#with open('/usr/users/nbeyer/clustersimulation/data/grid10_'+str(normalized_rate)+'.json', 'w') as outfile:
#    json.dump(result_dict, outfile)
