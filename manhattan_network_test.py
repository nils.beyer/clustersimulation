import numpy as np
import networkx as nx
from d3t.space import Network
import d3t
import pandas as pd    #'as pd' doesnt work use %px pd = pandas instead
from d3t.d3tsystem import D3TSystem
import devs
from d3t import statistics as st
from scipy import stats
from rideshared3t.eepa_dispatcher import EEPADispatcherModel as Ellipse
from d3t.observers import RawDataObserver
from matplotlib import pyplot as plt
import pickle
import random
from d3t.space import OSMNetwork
import csv
import sys



#graph
manhattan_fromsave = nx.DiGraph()
with open('/usr/users/nbeyer/clustersimulation/manhattan_Graph_connected.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
            manhattan_fromsave.add_edge(int(row[0]), int(row[1]), distance=float(row[2]), length=float(row[2])) #marszal used "length"

tspace = Network(manhattan_fromsave)

