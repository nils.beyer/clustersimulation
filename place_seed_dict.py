#!/usr/users/nbeyer/.conda/envs/d3t_3/bin/python
import os
from pathlib import Path
import networkx as nx
import json
import csv
import numpy as np
import random
import math
from datetime import timedelta #datetime,
import glob
import pandas as pd

#T_INVEHICLELIST = list(np.round(np.arange(1, 4.1, 0.2), 1))
def main():
    # Parameters
    REQUESTS = 10000
    TAXI_SPEED = 7.1 * 1609.3 / 3600  # 7.1 mp/h  according to http://www.nyc.gov/html/dot/downloads/pdf/mobility-report-2018-screen-optimized.pdf


    SEATLIST = [8]
    TRANSPORTERLIST = [1, 20]
    RATELIST = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 8.0]
    #[1.0, 2.0, 3.0, 5.0]#[0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 8.0]#list(np.round(np.arange(1.0, 21.0, 1.0), 2))#list(np.round(np.arange(2.5, 31.0, 2.5), 2))#
    absolute_rate = False
    #[0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 8.0]#list(np.round(np.arange(1.1, 2.0, 0.1), 3))#[0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 8.0]#list(np.round(np.arange(0.5, 8.1, 0.5), 3))#
    vary_detour = False

    T_MAXWAITLIST = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0]


    RESULT_DATA_ROOT_DIRECTORY = os.getcwd()
    #'/usr/users/nbeyer/clustersimulation/'
    #machine = '/home/nbeyer/test/'


# Set the following manually. Be careful not to overwrite existing folders(' seed dicts)

    overwrite = True #is that implemented? True is probably not implemented

    day = 3
    SIZE = 10 # means 10 * 10
    #directory = 'grid_100'
    #topology = 'grid_100'
    WEIGHT = 0.09
    directory = f'grid_100'#complete_graph_100'#'star_weighted_{WEIGHT}'#manhattan_uniform_2'#geometric_3'
    topology = f'grid_100'#complete_graph_100'#'star_weighted'#'manhattan_uniform'#'geometric_100' # 'triangular_13',

    simulationpath = os.path.join(RESULT_DATA_ROOT_DIRECTORY, 'data', directory)
    createDirectory(simulationpath)




    if topology == 'euclidian_plane':
        norm = 0.52141
    else:
        G = createGraph(topology, TAXI_SPEED, SIZE, WEIGHT, RESULT_DATA_ROOT_DIRECTORY)
        nx.write_gpickle(G, simulationpath + '/'+str(topology)+'.gpickle')
        if seed_exists(simulationpath, 10, 8, vary_detour=False):
            norm = read_norm(simulationpath)
        else:
            norm = createNorm(topology, TAXI_SPEED, G, RESULT_DATA_ROOT_DIRECTORY, day)
    if topology == 'manhattan_data':
        create_simulation_ready_taxi_frames(simulationpath, SEATLIST, TRANSPORTERLIST, RATELIST,
                                            RESULT_DATA_ROOT_DIRECTORY, day, norm)
    remaining_simulations = []
    for seats in SEATLIST:
        for transporters in TRANSPORTERLIST:
            ratelist_new = RATELIST.copy()
            if absolute_rate: # divide rates by N because they will be multiplied with N later
                ratelist_new = [np.round(rate/transporters, 2) for rate in ratelist_new]

            if seed_exists(simulationpath, transporters, seats, vary_detour):
                if overwrite == False:
                    ratelist_new = remove_completed_rates(simulationpath, transporters, seats, ratelist_new)
                for remaining_rate in ratelist_new:
                    remaining_simulations.append(str(transporters) + '_' + str(seats) + '_' + str(remaining_rate))
            else:
                createSeedDict(seats, transporters, simulationpath, norm, REQUESTS, vary_detour)
                for remaining_rate in ratelist_new:
                    remaining_simulations.append(str(transporters) + '_' + str(seats) + '_' + str(remaining_rate))
    if vary_detour:
        writeSubmitSkript(SEATLIST, TRANSPORTERLIST, RATELIST, directory, topology)
    else:
        writeSubmitSkriptFlexible(remaining_simulations, directory, topology)



def remove_completed_rates(simulationpath, transporters, seats, ratelist_new):
    # with open(seedpath, 'r') as infile:
    #     seed_dict = json.load(infile)
    #     ratelist_old = seed_dict['rates']
    #     # remove old rates from the new
    #     for rate_done in ratelist_old:
    #         try:
    #             ratelist_new.remove(rate_done)
    #         except ValueError:
    #             pass

    subpath = f'{simulationpath}/{transporters}/{seats}'
    completed_rates = [float((frame.split('/')[-1]).split('_')[0]) for frame in glob.glob(str(subpath) + '/*jobs*')]
    for rate_done in completed_rates:
        try:
            ratelist_new.remove(rate_done)
        except ValueError:
            pass
    return ratelist_new



def seed_exists(simulationpath, transporters, seats, vary_detour):
    if vary_detour:
        seedpath = f'{simulationpath}/{transporters}/{seats}/1.0/seeddict.json'
    else:
        seedpath = f'{simulationpath}/{transporters}/{seats}/seeddict.json'
    path_object = Path(seedpath)
    return path_object.is_file()

def frame_exists(subpath, normalized_rate):
    framepath = f'{subpath}/{normalized_rate}.csv'
    path_object = Path(framepath)
    return path_object.is_file()

def createNorm(topology, taxi_speed, G, root_folder, day):
    if topology == 'goettingen':
        norm = 5351.73 / taxi_speed
    elif topology.split('_')[0] == 'manhattan':
        norm = 7218.854300998714 / taxi_speed  # nx.average_shortest_path_length(G, 'distance') for uniformly distributed requests
        if topology == 'manhattan_data':
            #norm = 3429.849872100554 / taxi_speed  # norm for requests of 1.1.2011
	#norm = 1069.8053968232787 # for requests in 2.1.2011
            fullset = pd.read_csv(f'{root_folder}/taxi_data/2011_1_{day}_fully_mapped.csv',
                                  parse_dates=[' pickup_datetime', ' dropoff_datetime'],
                                  index_col=['Unnamed: 0'])
            path_lengths = []
            for i, row in (fullset).iterrows():
                path_lengths.append(
                    nx.shortest_path_length(G=G, source=row['newnode_pickup'],
                                            target=row['newnode_dropoff'],
                                            weight='distance'))
            norm = np.mean(path_lengths)
    elif topology == 'center':
        norm = 500.0 / 99.0
    else:
        norm = nx.average_shortest_path_length(G, weight='distance')
    return norm

def read_norm(simulationpath):
    print('dict exists')
    # dub path as 10/8 should always exist
    dict_path = os.path.join(simulationpath, '10', '8', 'seeddict.json')

    path_object = Path(dict_path)
    if path_object.is_file():
        with open(dict_path, 'r') as infile:
            seed_dict = json.load(infile)
            norm = seed_dict['norm']
    else:
        dict_path = os.path.join(simulationpath, '10', '1', 'seeddict.json')
        with open(dict_path, 'r') as infile:
            seed_dict = json.load(infile)
            norm = seed_dict['norm']
    return norm


def createGraph(topology, taxi_speed, size, weight, machine):
    if topology.split('_')[0] == 'manhattan' or topology == 'goettingen':
        city_fromsave = nx.DiGraph()
        if topology.split('_')[0] == 'manhattan':
            city = 'manhattan_Graph_cut'
        elif topology == 'goettingen':
            city = 'goettingen_graph_edges'
        with open(f'{machine}/data/{city}.csv',
                  'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in spamreader:
                city_fromsave.add_edge(int(row[0]), int(row[1]), distance=float(row[2]) / taxi_speed)
        G = city_fromsave
    elif topology.split('_')[0] == 'chain':
        G = nx.Graph()
        G.add_nodes_from(range(0, size*size -1))
        G.add_edges_from([(i,i+1) for i in range(size*size-2)])
    elif topology.split('_')[0] == 'grid':
        G = nx.grid_2d_graph(size, size)
    elif topology == 'star_weighted':
        G = nx.Graph()
        G.add_edges_from([(0, i) for i in np.arange(1,100,1)], distance = 1.0)
        G.add_edges_from([(i, i + 1) for i in np.arange(1, 99, 1)], distance = weight)
    elif topology.split('_')[0] == 'star':
        G = nx.star_graph(size*size-1)
    elif topology.split('_')[0] == 'complete':
        G = nx.complete_graph(size*size)
    elif topology == 'geometric_100':
        n = 100
        p = {i: (random.random(), random.random()) for i in range(n)}
        G = nx.random_geometric_graph(n, 0.2, pos=p)
        while(nx.is_connected(G)==False):
            p = {i: (random.random(), random.random()) for i in range(n)}
            G = nx.random_geometric_graph(n, 0.2, pos=p)
        points = []
        for i in p:
            points.append(p[i])
        for (u, v, w) in G.edges(data=True):
            w['distance'] = distance(points, u, v)
    elif topology == 'triangular_13':
        G = nx.generators.lattice.triangular_lattice_graph(13, 13, periodic=False)  # len(triangular.nodes) = 105
# setting distances/edgelengths depending on positions, not necessary here as they are equidistant (= 1)
        #points = {}
        #points.update({node: G.nodes(data='pos')[node] for node in G.nodes})
        #for (u, v, w) in G.edges(data=True):
        #    w['distance'] = distance(points, u, v)
    elif topology == 'hexagonal_6':
        G = nx.generators.lattice.hexagonal_lattice_graph(6, 6, periodic=False)  # len(hexagonal.nodes) = 96
    else:
        raise NameError("No Matching Topolgy")
    return G


def createSeedDict(seats, transporters, path, norm, requests, vary_detour):

    if vary_detour:
        # create all of simulation set
        for detour in T_INVEHICLELIST:
            subpath = f'{path}/{transporters}/{seats}/{detour}'
            createDirectory(subpath)
            seed_dict = {'seats': seats, 'norm': norm, 'transporters': transporters, 'tmax': requests, 'rates': [], 'detour': detour}
            with open(subpath + '/seeddict.json', 'w') as outfile:
                json.dump(seed_dict, outfile)
    else:
        # define the name of the directory to be created
        subpath = f'{path}/{transporters}/{seats}'
        createDirectory(subpath)
        seed_dict = {'seats': seats, 'norm': norm, 'transporters': transporters, 'tmax': requests, 'rates': []}
        with open(subpath + '/seeddict.json', 'w') as outfile:
                json.dump(seed_dict, outfile)
    return 0


def createDirectory(subpath):
    try:
        os.makedirs(subpath)  # can create subdirectories
    except OSError:
        print ("Creation of the directory %s failed" % subpath)
    else:
        print ("Successfully created the directory %s" % subpath)


def writeSubmitSkriptFlexible(remaining_simulations, directory, topology):
    file = open('submit_skript_network.sh','w')
    file.write('for i in '+(str(remaining_simulations)[1:-1]).replace(',','').replace('_', ',').replace('\'','')+';\n')
    file.write('do IFS=",";\n')
    file.write('\tset -- $i;\n')
    file.write('\techo "Seats: $2 Number of Transporters: $1 Rho: $3";\n')
    file.write(f'\tsbatch -n 1 -N 1 --mem=5G --open-mode=append -t 2-00:00:00 -o \'redooutput.txt\' --wrap=\"~/.conda/envs/d3t_3/bin/python cluster_simulation_grid.py $3 \'{directory}\' $1 $2 \'{topology}\'\";\n')
    file.write('done;\n')
    file.close()
    return 0

def writeSubmitSkript(seatlist, transporterlist, ratelist, directory, topology): #detour
    file = open('submit_skript_network.sh','w')
    file.write('for s in '+(str(seatlist)[1:-1]).replace(',','')+';\n')
    file.write('\tdo\n')
    file.write('\tfor d in '+(str(T_INVEHICLELIST)[1:-1]).replace(',','')+';\n')
    file.write('\t\tdo\n')
    file.write('\t\tfor k in '+(str(ratelist)[1:-1]).replace(',','')+';\n')
    file.write('\t\t\tdo\n')
    #file.write('\t\t\tfor t in '+(str(t_maxwaitlist)[1:-1]).replace(',','')+';\n')
    #file.write('\t\t\t\tdo\n')
    file.write('\t\t\techo "Seats: ${s} Number of Transporters: ${n} Rho: ${k}";\n')
    file.write(f'\t\t\tsbatch -n 1 -N 1 --mem=5G --open-mode=append -t 2-00:00:00 -o \'redooutput.txt\' --wrap=\"~/.conda/envs/d3t_3/bin/python cluster_simulation_grid.py $k \'{directory}\' 10 $s \'{topology}\' -invehicledelay $d\";\n')
    #file.write('\t\t\t\tdone;\n')
    file.write('\t\t\tdone;\n')
    file.write('\t\tdone;\n')
    file.write('\tdone;\n')
    file.close()
    return 0


def create_simulation_ready_taxi_frames(simulationpath, seatlist, transporterlist, ratelist, machine, day, norm):
    #does not replace old frames
    # doesnt catch 2 requests within 1 second leading to error in d3t -> gets caught in generator
    #norm = 3429.849872100554 / taxi_speed  # choose norm based on average path lengths
    fullset = pd.read_csv(f'{machine}/taxi_data/2011_1_{day}_fully_mapped.csv',
                          parse_dates=[' pickup_datetime', ' dropoff_datetime'],
                          index_col=['Unnamed: 0'])  # ' pickup_datetime'])#
    for seats in seatlist:
        for NTRANSPORTERS in transporterlist:
            for normalized_rate in ratelist:
                np.random.seed(1)
                subpath = simulationpath + '/' + str(NTRANSPORTERS) + '/' + str(seats)
                if frame_exists(subpath, normalized_rate):
                    pass
                else:
                    number_of_requests = int(normalized_rate * NTRANSPORTERS / (2 * norm) * 3600 * 24)  # requests to achieve rate
                    simulation_ready = createTaxiDay(fullset, number_of_requests)
                    createSeedDict(seats, NTRANSPORTERS, simulationpath, norm, number_of_requests, )
                    saveSimulationCsv(subpath, normalized_rate, simulation_ready)


def createTaxiDay(fullset, number_of_requests):
    gesamtrequests = len(fullset)
    fullsamplelist = np.random.choice(np.arange(gesamtrequests), size=number_of_requests, replace=True, p=None)  # create indices to select random sample
    fullsamplelist.sort()  # necessary to get correct timedeltas
    day = fullset.iloc[fullsamplelist]  # create dataframe by sample indices
    differences = ((day[' pickup_datetime']).diff(periods=1).shift(-1))  # differences of datetime objects
    differences.replace(to_replace=pd.NaT, value=timedelta(1), inplace=True)  # replace last entry NaT with high number so simulation can finish
    day_seconds = differences.apply(lambda row: row.total_seconds())  # convert to float seconds
    day = day.assign(differences=differences)
    simulation_ready = day.assign(seconds_to_next=day_seconds)
    return simulation_ready


def saveSimulationCsv(subpath, normalized_rate, simulation_ready):
    with open(str(subpath) + '/' + str(normalized_rate) + '.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for index, row in simulation_ready.iterrows():
            spamwriter.writerow([row['newnode_pickup'], row['newnode_dropoff'], row['seconds_to_next']])


def distance(points, i, j):
    dx = points[i][0] - points[j][0]
    dy = points[i][1] - points[j][1]
    return math.sqrt(dx * dx + dy * dy)


if __name__ == "__main__":
    main()
