import networkx as nx
from d3t.space import Network
import pickle
import csv

taxi_speed = 7.1*1609.3/3600
city_fromsave = nx.DiGraph()
#with open('data/manhattan_Graph_cut.csv', 'r', newline='') as csvfile:
with open('data/goettingen_graph_edges.csv', 'r', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        city_fromsave.add_edge(int(row[0]), int(row[1]), distance=float(row[2]) / taxi_speed)
#G= nx.read_gpickle('data/nxmanhattan_cut.gpkl')
tspace = Network(city_fromsave)
clean_distance = {k:dict(v) for k,v in tspace._distances.items()} #to get rid of default_dict

#and pickle clean dict 
with open("/usr/users/nbeyer/clustersimulation/data/Network_goettingen_distances.p","wb") as f:
    pickle.dump(clean_distance,f)
    
predecessors = tspace._predecessors

with open("/usr/users/nbeyer/clustersimulation/data/Network_goettingen_predecessors.p","wb") as f:
    pickle.dump(predecessors,f)
