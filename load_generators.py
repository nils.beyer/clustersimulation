import numpy as np
import networkx as nx
import random
from random import sample
#from itertools import islice


def load_generator_network_uniform(rate, G): #works for all networks INCLUDING grids
    t=0
    while True:
        tdelta = np.random.exponential(scale=1.0 / rate)
        src, dest = random.sample(G.nodes(), 2)
        #src, dest = np.random.choice(range(len(G.nodes())), size=2, replace=False)
        #src=list(G)[src]
        #dest=list(G)[dest]
        t += tdelta
        yield (
            tdelta,
            src, dest
        )
	
def load_generator_network_cutoff(rate, G, cutoff): #works for grids #cutoff is the highest distance included
    t=0
    while True:
#        if t>5.:
#            rate = rate
#        else:
#            rate=2*rate
        tdelta = np.random.exponential(scale=1.0 / rate)
        src, dest = random.sample(G.nodes(), 2) #won't pick the same node twice
        while (np.abs(src[0]-dest[0]) +np.abs(src[1]-dest[1]) > cutoff):# and np.abs(src[0]-dest[0]) +np.abs(src[1]-dest[1]) != 0):
            src, dest = random.sample(G.nodes(), 2)
        t += tdelta
        yield (
            tdelta,
            src, dest
        )
   
	
def load_generator_network_corner(rate, G): #grid upper left
    t=0
    while True:
#        if t>5.:
#            rate = rate
#        else:
#            rate=2*rate
        tdelta = np.random.exponential(scale=1.0 / rate)
        src, dest = (0,0),random.sample(G.nodes(), 1)[0]
        t += tdelta
        yield (
            tdelta,
            src, dest
        )
	

def load_generator_network_center(rate, G): #grid center#norm = 500/99 #center norm

    t=0
    while True:
#        if t>5.:
#            rate = rate
#        else:
#            rate=2*rate
        tdelta = np.random.exponential(scale=1.0 / rate)
        src, dest = (4,4),random.sample(G.nodes(), 1)[0]
        while (dest == (4,4)):
            dest = random.sample(G.nodes(), 1)[0]
        t += tdelta
        yield (
            tdelta,
            src, dest
        )
        
def load_generator_from_data(dataframe):
    for index, row in dataframe.iterrows():
        tdelta = row['seconds to next']
        if tdelta < 0.00001:
            tdelta = 0.00001
            print('delta = 0 encountered')
        yield (tdelta, row['start id'],row["dest id"])

def load_generator_r2_uniform_adaptive(rate):
    t = 0
    
    while True:
        if t>0.:
            lam = rate
        else:
            lam=2*rate

        tdelta = np.random.exponential(scale=1.0 / lam)
        t+=tdelta

        yield (
            tdelta,
            np.random.uniform(0.0, 1.0, size = 2), np.random.uniform(0.0, 1.0, size = 2),
        )
def load_generator_r2_uniform(rate):
    t = 0
    
    while True:
        tdelta = np.random.exponential(scale=1.0 / rate)
        t+=tdelta
        yield (
            tdelta,
            np.random.uniform(0.0, 1.0, size = 2), np.random.uniform(0.0, 1.0, size = 2),
        )
