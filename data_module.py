# This module contains the functions for loading and annotating my data in
# Datadict and Metadict

import os
import matplotlib as mpl
import glob
import json
import numpy as np
import matplotlib.cm as cm
#TODO:
def createDataDict(finishedSimulations, root_folder, defaulttotal = 6.0, defaultinvehicle = 2.0):
    """
    Create a dictionary with all simulation results
    :param finishedSimulations:
    list of all simulated topologies
    :param root_folder:
    path to root of resultfolder
    :return:
    dict of all simulations
    keys: topology_transporters_seats_invehicletime_totaltime
    values: all observables mapped to 'rate' also included in the dict
         "rate": [0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 5.0, 7.0, 8.0]
    """

    combineddict = {}
    for topology_folder in finishedSimulations:
        path = os.path.join(root_folder, 'results', 'data', topology_folder)
        for transporters in [int(folder.split('/')[-1]) for folder in glob.glob(f'{path}/*')]:
            for seats in [int(folder.split('/')[-1]) for folder in glob.glob(f'{path}/{transporters}/*')]:
                if topology_folder == 'grid_t_invehicle':
                    for time in list(np.round(np.arange(1, 4.1, 0.2), 1)):
                        with open(f'{path}/{transporters}/{seats}/{time}/resultdict.json','r') as readfile:
                            data = json.load(readfile)
                        combineddict.update({f'{topology_folder}_{transporters}_{seats}_{time}_{defaulttotal}' : data})
                elif topology_folder == 'grid_t_totaldelay':
                    for time in [1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0]:
                        with open(f'{path}/{transporters}/{seats}/{time}/resultdict.json', 'r') as readfile:
                            data = json.load(readfile)
                            combineddict.update({f'{topology_folder}_{transporters}_{seats}_{defaultinvehicle}_{time}' : data})
                elif topology_folder == 'geometric':
                    pass
                else:
                    try:
                        with open(
                                f'{path}/{transporters}/{seats}/resultdict.json',
                                'r'
                        ) as readfile:
                            data = json.load(readfile)
                        combineddict.update({f'{topology_folder}_{transporters}_{seats}_{defaultinvehicle}_{defaulttotal}'
                                             : data})
                    except FileNotFoundError:
                        print('Data for '+f'{path}/{transporters}/{seats}/resultdict.json could not be loaded')
    return combineddict


def createMetaDict(finishedSimulations, root_folder, observe_list):
    """
    Create a dictionary with specifications of how to plot things


    :param finishedSimulations:
    list of all simulated topologies
    :param root_folder:
    path to root of resultfolder
    :return:
    Dict with keys: f'{topology}_{parameter}_{observe}':
    and a dictionary as value:
        {'seats': seats,
         'transporters': transporters,
         'color': color,
         'topology': topology,
         'observe': observe,
         'parameter': parameter,
         'marker': marker,
         'xaxis': xaxis, 'xrange': xrange,
         'yaxis': yaxis, 'yrange': yrange,
         'title': title,
         'ratelist': ratelist,
         'invehicle_t':invehicle_t,
         'total_t': total_t}}
    """
    metaData = {}
    for observe in observe_list:
        for topology in finishedSimulations:
            if topology == 'grid_t_invehicle':
                updateMetaData(metaData, topology, observe, 'invehicle_t', root_folder)
            elif topology == 'grid_t_totaldelay':
                updateMetaData(metaData, topology, observe, 'totaldelay', root_folder)
            else:
                for parameter in ['rate']:
                    updateMetaData(metaData, topology, observe, parameter, root_folder)
                if topology == 'grid_100':
                    for parameter in ['ntaxis', 'unnormed_rate']:
                        updateMetaData(metaData, topology, observe, parameter, root_folder)


        updateMetaData(metaData, 'grid_100', observe, 'only_rate', root_folder)
    return metaData


def updateMetaData(metaData, topology, observe, parameter, root_folder):
    """
    Update the metadata dict supplied as argument in place according to given topology, observable and parameter
    :param root_folder:
    path to root of resultfolder
    """

    seats = determine_seats(parameter, root_folder, topology)
    transporters = determine_transporters(parameter, topology, root_folder)
    color = determine_color(parameter, transporters)
    markerdict = {'1':(3, 1), '8':(2, 1), '25': (4, 1), '100': (5, 1), '400': (6,1)}
    yaxis, yrange = determine_y_axis(observe)
    xaxis, xrange =  determine_x_axis(parameter)
    title = determine_title(topology)
    ratelist = determine_ratelist(parameter, topology)

    if parameter == 'rate' or parameter == 'ntaxis' or parameter == 'rejection_ratio' or parameter == 'only_rate' or parameter == 'unnormed_rate':
        invehicle_t = [2.0]
        total_t = [6.0]
    elif parameter == 'invehicle_t':
        invehicle_t = list(np.round(np.arange(1, 4.1, 0.2), 1))
        total_t = [6.0]
    elif parameter == 'totaldelay':
        invehicle_t = [2.0]
        total_t = [1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0]
    else:
        raise ValueError("No Matching Parameter")

    metaData.update({f'{topology}_{parameter}_{observe}':{'seats': seats,
                                                            'transporters': transporters,
                                                            'color': color,
                                                            'topology': topology,
                                                            'observe': observe,
                                                            'parameter': parameter,
                                                            'marker': markerdict,
                                                            'xaxis': xaxis, 'xrange': xrange,
                                                            'yaxis': yaxis, 'yrange': yrange,
                                                            'title': title,
                                                            'ratelist': ratelist,
                                                            'invehicle_t':invehicle_t,
                                                            'total_t': total_t}})


def determine_seats(parameter, path, topology):
    if parameter == 'unnormed_rate' or topology == 'geometric_2':
        seats = [8]
    elif topology == 'grid_100':
        seats = [1, 8]
    elif parameter == 'rate':
        seats = [8]  # [int(folder.split('/')[-1]) for folder in glob.glob(f'{path}/results/data/{topology}/10/*')]
    # [float((element.split('/')[-1]).split('_')[0]) for element in glob.glob(f'{path}/results/data/{topology}/10/8/*jobs*')] #10 is arbitrary but robust
    # [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
    # rates available depend on seats and transporters
    elif topology == 'manhattan_uniform_old_but_fine':
        seats = [1, 8]
    elif parameter == 'invehicle_t' or parameter == 'totaldelay':
        seats = [8]
    elif parameter == 'ntaxis' or parameter == 'only_rate':
        seats = [1, 8]
    elif parameter == 'rejection_ratio':
        seats = [8]
    else:
        raise AssertionError(f'No seats determined for {parameter} in {topology}')
    return seats


def determine_ratelist(parameter, topology):
    if parameter == 'ntaxis' or parameter == 'only_rate' or parameter == 'rate' or parameter == 'unnormed_rate': # gets determined automatically
        ratelist = None
#    elif parameter == 'rate':
#        ratelist = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
    elif parameter == 'rejection_ratio':
        ratelist = [5.0]
    elif parameter == 'invehicle_t':
        ratelist =  [1.0, 2.0, 3.0, 5.0] # [1, 2, 3, 4]  #
    elif parameter == 'totaldelay':
        ratelist = [1.0, 2.0, 3.0, 5.0]  # [1, 3, 5, 7]
    else:
        raise AssertionError('No ratelist found')
    return ratelist


def determine_title(topology):
    if topology == 'grid_100' or topology == 'grid_t_invehicle' or topology == 'grid_t_totaldelay' or topology == 'grid_ellipse_10':
        title = f'10\\,x\\,10 Grid'
    elif topology == 'grid_25':
        title = '5\\,x\\,5 Grid'
    elif topology == 'grid_400':
        title = '20\\,x\\,20 Grid'
    elif topology.split('_')[0] == 'chain':
        title = f"Chain Network containing {topology.split('_')[1]} Nodes"
    elif topology.split('_')[0] == 'star':
        title = f"Star Network containing {topology.split('_')[1]} Nodes" # for wheels this leads to Star Network containing weighted Nodes
    elif topology == 'euclidian':
        title = 'Euclidean Plane'
    elif topology.split('_')[0] == 'star':
        title = 'Wheel Graph'
    elif topology.split('_')[0] == 'manhattan':
        title = 'Manhattan Network'
    elif topology.split('_')[0] == 'geometric':
        title = 'Geometric Graph'
    elif topology == 'correlation/center':
        title = 'Grid with center as only Destination'
    elif topology == 'triangular':
        title = 'Triangular Lattice'
    elif topology == 'hexagonal':
        title = 'Hexagonal Lattice'
    elif topology == 'correlation_center':
        title = '$10 x 10$ Grid with all requests starting in the center'
    elif topology == 'geometric_2':
        title = 'Geometric Graph with 100 Nodes'
    else:
        raise NameError("No Matching Topology")
    return title


def determine_y_axis(observe):
    if observe == 'occupancy':
        yaxis, yrange = 'Average Occupancy', [0, 5.4]
    elif observe == 'rejection_ratio':
        yaxis, yrange = 'Rejection Ratio', [0, 1]
    elif observe == 'public_good':
        yaxis, yrange = 'Efficiency', [0, 5]
    elif observe == 'relative_waiting_time':
        yaxis, yrange = 'Average Relative Waiting Time', None # $[t_{\mathrm{direct}}]$
    elif observe == 'absolute_waiting_time':
        #yaxis, yrange = 'Average Waiting Time $[\\frac{l_{avg}}{v}]$', None
        yaxis, yrange = 'Average Waiting Time', None # $\cdot \\frac{v}{l_{avg}}$
    elif observe == 'travel_time':
        #yaxis, yrange = 'Relative Travel Time $[t_{\mathrm{direct}}]$', [1, 2]
        yaxis, yrange = 'Relative Travel Time $\cdot \\frac{1}{t_{\mathrm{direct}}}$', [1, 2]
    elif observe == 'absolute_accepted':
        yaxis, yrange = 'Absolute Number Accepted', None
    elif observe == 'absolute_rejected':
        yaxis, yrange = 'Absolute Number Rejected', None
    else:
        raise NameError("No Matching Observable")
    return yaxis, yrange


def determine_x_axis(parameter):
    if parameter == 'rate' or parameter == 'only_rate':
        #deb doesnt like:
        #xaxis = '$\\lambda [\\frac{v \\cdot N }{ 2 \\cdot l_{avg}}]$'
        #so we do:
        #xaxis = '$\\lambda \\frac{2 \\cdot l_{avg}}{v \\cdot N  }$'
        xaxis = '$\\tilde{\\lambda} / N$'#'Normalized Request Rate $\\tilde{\\lambda}$'
        xrange = [0, 8]
    elif parameter == 'invehicle_t':
        xaxis = 'Maximum Relative Detour'
        xrange = [-0.0, 3.1]
    elif parameter == 'totaldelay':
        xaxis = 'Maximum Relative Total Delay'
        xrange = None
    elif parameter == 'ntaxis':
#        xaxis = '$\\lambda [\\frac{v}{ 2 \\cdot l_{avg}}]$'
        xaxis = 'Normalized Request Rate $\\tilde{\\lambda}$'#'$\\lambda \cdot \\frac{2 \\cdot l_{avg}}{v}]$'
        xrange = [0, 80]
    elif parameter == 'unnormed_rate':
        xaxis = 'Normalized Request Rate $\\tilde{\\lambda}$'#'$\\lambda \cdot \\frac{2 \\cdot l_{avg}}{v}$'
        xrange = [0, 16]
    else:
        raise ValueError("No Matching Parameter")
    return xaxis, xrange


def determine_transporters(parameter, topology, path):
    transporters = None
    if parameter == 'rate':
        transporters = [int(folder.split('/')[-1]) for folder in glob.glob(f'{path}/results/data/{topology}/*')]
        if topology == 'grid_100':
            transporters = [1, 10, 20]
        elif topology == 'chain_100':
            transporters = [1, 10, 20]
        elif topology == 'euclidian':
            transporters = [1, 10, 20]
        elif topology == 'star_100':
            transporters = [1, 10, 20]
        elif topology.split('_')[0] == 'star':
            transporters = [1, 10, 20]
        elif topology == 'geometric_2':
            transporters = [1, 10, 20]
        elif topology == 'triangular':
            transporters = [1, 10, 20]
        elif topology == 'hexagonal':
            transporters = [1, 10, 20]
        elif topology == 'manhattan_uniform_old_but_fine':
            transporters = [1, 10, 20]  # N = 1, s = 1 missing
    elif parameter == 'invehicle_t':
        transporters = [10]
    elif parameter == 'totaldelay':
        transporters = [10]
    elif parameter == 'ntaxis':
        transporters = [10, 20, 25, 30, 40]
    elif parameter == 'rejection_ratio':
        transporters = [10]
    elif parameter == 'only_rate':
        transporters = [10]
    elif parameter == 'unnormed_rate':
        transporters = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        #else:
        #    raise ValueError("No Matching Parameter")
    return transporters

def determine_color(parameter='rate', transporters=[1,10,20]):
    # noinspection PyUnresolvedReferences
    norm = mpl.colors.Normalize(vmin=np.log2(1), vmax=np.log2(70))
    # noinspection PyUnresolvedReferences
    cmap = cm.magma
    #nipy_spectral  # gnuplot2
    m = cm.ScalarMappable(norm=norm, cmap=cmap)
    if parameter == 'ntaxis':
        norm = mpl.colors.Normalize(vmin=10, vmax=50)
        m = cm.ScalarMappable(norm=norm, cmap=cmap)
        color = {}
        for t in transporters:
            color.update({t:m.to_rgba(t)})
    else:
        #if parameter == 'rate' or parameter == 'only_rate' or parameter == 'rejection_ratio' or parameter == 'invehicle_t' or parameter == 'totaldelay' or parameter == 'unnormed_rate':
        color = {'chain_100': 'black', 'euclidian': 'blue', 'grid_100': 'mediumseagreen', 'star_100': 'darkred',
                 'grid_25': 'darkgreen', 'grid_400': 'dodgerblue', 'star_25': 'orangered', 'chain_25': 'grey',
                 #for service quality:
                 0.5: 'black', 1.0: 'blue', 2.0: 'green', 2.5: 'yellow', 3.0: 'orange', 5.0: 'red'
}
        for t in transporters:
            color.update({t: m.to_rgba(np.log2(t))})


    return color
