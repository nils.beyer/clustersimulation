import matplotlib as mpl

import matplotlib.pyplot as plt
import tikzplotlib
from tikzplotlib import save as tikz_save
from data_module import *

#from . import legend


OBSERVELIST = ['occupancy', 'rejection_ratio', 'public_good', 'travel_time',
               'absolute_waiting_time']  # ,'absolute_accepted']#, 'absolute_rejected'] #, 'idletime' not interesting
# travel time is relative travel time:q ##data for 'relative_waiting_time', actually absolute
DEFAULTINVEHICLE = 2.0
DEFAULTTOTAL = 6.0
MARKER_DICT = {'1': (3, 1), '8': (2, 1), '25': 's', '100': 'o', '400': 'd'}
# markers for combinations:
MARKER_DICT.update({'grid_100': (2, 1), 'euclidian': (3, 1), 'chain_100': (4, 1), 'star_100': (5, 1), 'triangular': (1, 1), 'hexagonal': (7,1),
                    'geometric_2':(8,1),'geometric_3':(8,1),'geometric_4':(8,1),
                    'manhattan_data_2011_1_1':(9,1), 'manhattan_data_2011_1_2':(9,1), 'manhattan_data_2011_1_3':(9,1), 'manhattan_uniform_2':(9,1)})
# pyplot.plot markers:
MARKER_DICT.update({'8': "s", '1': "D", 'grid_100': "s", 'grid_25': "s", 'grid_400': "s", 'euclidian': "o", 'chain_100': "d", 'star_100': "*"})
MARKER_DICT.update({'star_weighted_0.1_indexbug': 'x', 'star_weighted_0.2': 'x','star_weighted_0.5': 'x', 'star_weighted_0.04':'x', 'star_weighted_0.09':'x'})
COLORS  = {'chain_100': 'black', 'euclidian': 'blue', 'grid_100': 'mediumseagreen', 'star_100': 'darkred',
                 'grid_25': 'darkgreen', 'grid_400': 'dodgerblue', 'star_25': 'orangered', 'chain_25': 'grey',
#            'grid_100': 'green',
           2.5: 'yellow', 3.0: 'orange',
         'star_100': 'darkred',
         5.0: 'red', 'grid_25': 'lightgreen', 'star_25': 'orangered', 'chain_25': 'grey'}

norm_weight = mpl.colors.Normalize(vmin=0, vmax=1.1)
#norm_weight_log = mpl.colors.Normalize(vmin=np.log2(0+1), vmax=np.log2(1+1.1))
cmap = cm.magma#PuBuGn#viridis
#Reds#cividis
m = cm.ScalarMappable(norm=norm_weight, cmap=cmap)
w_0 = [0.04, 0.04, 0.09, 0.1, 0.15, 0.2, 0.5, 1]
w_log = [elem + 1 for elem in w_0]
w = np.log2(w_log)
print(w)
COLORS.update({'chain_100': m.to_rgba(w[0]), 'star_weighted_0.04':m.to_rgba(w[1]), 'star_weighted_0.09':m.to_rgba(w[2]),
               'star_weighted_0.1_indexbug':m.to_rgba(w[3]), 'grid_100':m.to_rgba(w[4]), 'star_weighted_0.2':m.to_rgba(w[4]), 'star_weighted_0.5':m.to_rgba(w[5]), 'star_100':m.to_rgba(w[6])})


RESULT_DATA_ROOT_DIRECTORY = os.getcwd()
SAVEMODE = ['pdf', 'tikz'][1]


def main():
    savedir = os.path.join(RESULT_DATA_ROOT_DIRECTORY, 'new_plots')
    createdirectories(savedir)
    # mulitple lists to speed up debugging
    finishedSimulations = ['grid_100',
                             'grid_25', 'grid_400', 'chain_100', 'chain_25', 'star_100', 'star_25',
                           'star_400', 'euclidian',
                           'geometric_2', 'geometric_3', 'geometric_4',
                           'triangular', 'hexagonal',
                           'star_weighted_0.1_indexbug', 'star_weighted_0.2', 'star_weighted_0.04', 'star_weighted_0.2_indexbug',
                           'star_weighted_0.5', 'star_weighted_0.09',
                           'grid_t_totaldelay', 'grid_t_invehicle',
                           'manhattan_data_2011_1_1', 'manhattan_data_2011_1_2','manhattan_data_2011_1_3',
                            'manhattan_uniform_2',#_old_but_fine',
                           #                          'correlation_center'
    ]  # 'comparison_10',
    #  'manhattan_data',
    # finishedSimulations = [simulation.split('/')[-1] for simulation in
    #                       glob.glob(os.path.join(RESULT_DATA_ROOT_DIRECTORY, 'results', 'data', '*'))]
    # plot_list = ['toy_1', 'toy_8', 'combine_grid_euclidean']
    dataDict = createDataDict(finishedSimulations, RESULT_DATA_ROOT_DIRECTORY)
    metaData = createMetaDict(finishedSimulations, RESULT_DATA_ROOT_DIRECTORY, OBSERVELIST)

    plot_all(dataDict, metaData, savedir, top_par_obs=False,#True,
             #combinationslist=['euclidian', 'chain_100', 'star_100', 'triangular', 'hexagonal', 'manhattan_uniform_2'],
             #ontheway=True,coarse_grain=True,
             #toy_1=False, only_taxis=True,
             errorbars = True,
             #wheel = True
             )  # , )#'toy_8_rejection_ratio_on_the_way_ness''toy_8_rate_rejection_ratio',['all_rate_rejection_ratio'])#, ['grid_100_rate_occupancy'])#,, 'grid_t_invehicle_invehicle_t_rejection_ratio','grid_ntaxis_rejection_ratio', 'grid_rate_public_good', ]) #'all_rate_rejection_ratio'
#top_par_obs=['manhattan_uniform_2_rate_rejection_ratio']


#'grid_100_unnormed_rate_absolute_waiting_time', 'grid_100_unnormed_rate_public_good', 'grid_100_unnormed_rate_occupancy'
# 'grid_t_invehicle_invehicle_t_public_good'
# 'grid_100_ntaxis_rejection_ratio'
# 'grid_100_only_rate_rejection_ratio'
# 'grid_100_unnormed_rate_absolute_rejected'

def plot_all(dataDict, metaDict, save_dir, top_par_obs, combinationslist=[], ontheway=False, coarse_grain=False, toy_1=False, only_taxis=False, errorbars=False, wheel=False):
    """
    Plots results of all specified folders by iterating over specifications from metaDict and feeding to plot function
    :param save_dir: Here we save the plots.
    :param dataDict: This contains all results.
    :param metaDict: This contains specifications for each result, how it should be plotted.
    :param top_par_obs: (topology_parameter_observe) : List of combinations to be plotted.
        If empty plot all items from metaDict
    :return:
    """


    if ontheway:
        ontheway_plot(dataDict, metaDict, save_dir)
    if errorbars:
        for observe in OBSERVELIST:
            errorbars_plot(dataDict, metaDict, observe, save_dir)

    # Compare how similar Taxis are
    if toy_1:
        list_of_xy_pairs = []
        seats = 1
        observe = 'rejection_ratio'
        for network in ['grid', 'star', 'chain']:
            for size in [25, 100, 400]:
                topology = f'{network}_{size}'
                label = determine_legend(topology)
                for transporters in [10]:
                    try:
                        plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                        ratelist = plotdict['rate']
                        x_values, y_values = select_x_y(plotdict, ratelist, observe)
                        list_of_xy_pairs.append([x_values, y_values, label,
                                                 [COLORS[f'{network}_100']], MARKER_DICT[str(size)]])
                        print(f'seats: {seats}_transporters: {transporters}_topology: {topology} done')
                    except KeyError:
                        print(
                            f'No Data for topology comparison {topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}')
        specifications_toy = metaDict['grid_100_rate_rejection_ratio']
        specifications_toy.update({'title': 'Comparison of different Topologies','topology': 'toy_1'})
        plot(specifications_toy, save_dir, list_of_xy_pairs)


    for combination in combinationslist:
        for observe in OBSERVELIST:
            key_other = f'{combination}_rate_{observe}'
            specifications = metaDict[key_other]
            topology = f'combine-{combination}'
            # specifications_other is given to plot
            title = f'Comparison of Grid and {determine_legend(combination)}'
            specifications.update({'topology': topology, 'marker': MARKER_DICT, 'title': title, 'transporters':[1,10,20]})

            list_of_xy_pairs = prepare_x_y(dataDict, specifications, metaDict)
            plot(specifications, save_dir, list_of_xy_pairs)

    if coarse_grain:
        for observe in OBSERVELIST:
            coarse_grain_or_wheel_plot(dataDict, observe, save_dir, [1, 10, 20], ['grid_25', 'grid_100', 'grid_400', 'euclidian'], 'coarse_grain')

    if wheel:
        for observe in OBSERVELIST:
            coarse_grain_or_wheel_plot(dataDict, observe, save_dir, [1, 10], ['star_100', 'star_weighted_0.2','star_weighted_0.09','grid_100', 'star_weighted_0.04','chain_100'], 'wheel')#'star_weighted_0.5', 'star_weighted_0.1_indexbug',
    if only_taxis:
        for observe in OBSERVELIST:
            first_taxi_plot(dataDict, metaDict, observe, save_dir)
    if top_par_obs == True:
        for key, dictionary in metaDict.items():
            if key.split('_')[0]=='geometric' or key.split('_')[0]=='manhattan': #(seperate case)
                break
            list_of_xy_pairs = prepare_x_y(dataDict, metaDict[key], metaDict)
            plot(dictionary, save_dir, list_of_xy_pairs)
    elif top_par_obs == False:
        pass
    else:
        for key in top_par_obs:
            if key.split('_')[0]=='geometric': #(seperate case)
                break
            simulation_dict = metaDict[key]
            list_of_xy_pairs = prepare_x_y(dataDict, simulation_dict, metaDict)
            plot(simulation_dict, save_dir, list_of_xy_pairs)


def prepare_x_y(dataDict, specifications, metaDict):
    """
    prepare x and y data for 1 plot
    :param dataDict: Dictionary of all data (could be optimized), from which, we pick some dictionaries
     based on specifications of the form {topology}_{transporters}_{seats}_{invehicle}_{total}
     those dictionaries hold all observables mapped to 'rate' also included in the dict
     "rate": [0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 5.0, 7.0, 8.0]
    :param specifications:
    :return: pairs of lists that will plotted together
    """

    parameter = specifications['parameter']
    observe = specifications['observe']
    topology = specifications['topology']
    seatlist = specifications['seats']
    transporterlist = specifications['transporters']
    RATELIST = specifications['ratelist']
    invehicle_t = specifications['invehicle_t']
    total_t = specifications['total_t']
    markerdict = MARKER_DICT
    colors = specifications['color']
    list_of_xy_pairs = []

    if topology.split('-')[0] == 'toy' or observe == 'on_the_way_ness' or topology.split('-')[0] == 'combine':
        list_of_xy_pairs = compare_topologies(dataDict, specifications, metaDict)

    else:
        for seats in seatlist:
            try:
                ratelist = RATELIST.copy()
            except AttributeError:
                pass  # ratelist = None

            if parameter == 'ntaxis':
                if seats == 8:
                    transporters = 10
                    label = determine_Nlabel(seats, transporters)
                    plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                    ratelist = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
                    indices = [plotdict['rate'].index(rate) for rate in ratelist]
                    ratelist = [rate * transporters for rate in ratelist]  # unnormalize x-axis
                    if observe == 'absolute_accepted':
                        y_values = [(1 - plotdict['rejection_ratio'][index]) * plotdict['rate'][index] for
                                    index in indices]
                    elif observe == 'absolute_rejected':
                        y_values = [(plotdict['rejection_ratio'][index]) * plotdict['rate'][index] for
                                    index in indices]
                    else:
                        y_values = [plotdict[observe][index] for index in indices]
                    x_values = ratelist.copy()
                    check_lenghts(x_values, y_values, topology)
                    list_of_xy_pairs.append([x_values, y_values, label, [colors[transporters]], markerdict[str(seats)]])
                elif seats == 1:
                    for transporters in transporterlist:
                        label = determine_Nlabel(seats, transporters)
                        # ratelist = [rate * (10.0/transporters) for rate in ratelist] # select different values not strictly necessary -> cut off x
                        plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                        if RATELIST == None:
                            ratelist = plotdict['rate'].copy()
                        ratelist_copy = ratelist.copy()
                        ratelist_copy.sort()  # we need to order the rates to prevent rogue connections in line plots
                        indices = [plotdict['rate'].index(rate) for rate in ratelist_copy]
                        ratelist_scaled = [rate * transporters for rate in ratelist_copy]  # unnormalize x-axis

                        if observe == 'absolute_accepted':
                            y_values = [(1 - plotdict['rejection_ratio'][index]) * plotdict['rate'][index] for index in
                                        indices]
                        elif observe == 'absolute_rejected':
                            y_values = [(plotdict['rejection_ratio'][index]) * plotdict['rate'][index] for index in
                                        indices]
                        else:
                            y_values = [plotdict[observe][index] for index in indices]
                        x_values = ratelist_scaled.copy()
                        check_lenghts(x_values, y_values, topology)
                        list_of_xy_pairs.append(
                            [x_values, y_values, label, [colors[transporters]], markerdict[str(seats)]])

            else:
                for transporters in transporterlist:
                    if (parameter == 'invehicle_t') or (parameter == 'totaldelay'):
                        if observe == 'absolute_accepted' or observe == 'absolute_rejected':
                            break
                        if parameter == 'invehicle_t':
                            t_x = [time - 1 for time in
                                   invehicle_t]  # because 'delay' usually refers to relative time -1
                        elif parameter == 'totaldelay':
                            t_x = [time - 1 for time in total_t]
                        for rate in ratelist:
                            label = f'$\\lambda = {rate} \lambda_c$'
                            plotlist = []
                            for t_in in invehicle_t:
                                for t_total in total_t:
                                    plotdict = dataDict[f'{topology}_{transporters}_{seats}_{t_in}_{t_total}']
                                    index = plotdict['rate'].index(rate)
                                    y = plotdict[observe][index]
                                    plotlist.append(y)
                            x_values = t_x.copy()
                            y_values = plotlist.copy()
                            check_lenghts(x_values, y_values, topology)
                            list_of_xy_pairs.append([x_values, y_values, label, [colors[rate]], markerdict[str(seats)]])



                    elif parameter == 'rate' or parameter == 'only_rate' or parameter == 'unnormed_rate':
                        label = determine_Nlabel(seats, transporters)

                        try:
                            plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                        except KeyError:
                            print(f'No data found in Datadict for {topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}')
                            break

                        if RATELIST == None:
                            ratelist = plotdict['rate'].copy()

                        indices = []

                        all_measured_rates = plotdict['rate'].copy()
                        ratelist_copy = ratelist.copy()
                        ratelist_copy.sort()  # we need to order the rates to prevent rogue connections in line plots
                        for rate in ratelist_copy:  # look where the rates where are, and save their postions in 'indices'
                            try:
                                rate_index = all_measured_rates.index(rate)
                                indices.append(rate_index)
                            except ValueError:
                                ratelist.remove(
                                    rate)  # plot data that exists # ACTUALLY REMOVES RATE ALSO FROM list_of_xy_pairs, where was appended earlier!!!!!!!!!!!!
                                print(f'{rate} is not in the list of {topology}_{transporters}_{seats}')
                        if observe == 'absolute_accepted':
                            y_values = [
                                transporters * (1 - plotdict['rejection_ratio'][index]) * plotdict['rate'][index] for
                                index in indices]
                        elif observe == 'absolute_rejected':
                            y_values = [transporters * (plotdict['rejection_ratio'][index]) * plotdict['rate'][index]
                                        for index in indices]
                        else:
                            y_values = [plotdict[observe][index] for index in indices]
                        if (parameter == 'unnormed_rate'):
                            x_values = [transporters * rate for rate in ratelist_copy]
                        else:
                            x_values = ratelist_copy.copy()
                        #                        x_values.sort()
                        check_lenghts(x_values, y_values, topology)
                        list_of_xy_pairs.append(
                            [x_values, y_values, label, [colors[transporters]], markerdict[str(seats)]])
    return list_of_xy_pairs


def prepare_standard_x_y(topology, transporters, dataDict, observe, color='topology'):
    '''
    rate as x
    obs as y
    sorting
    :param topology:
    :return:
    '''
    seats = 8
    label = determine_legend(topology)
    plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
    ratelist = plotdict['rate'].copy()
    x_values, y_values = select_x_y(plotdict, ratelist, observe)
    check_lenghts(x_values, y_values, topology)
    colordict = determine_color('rate', [1,10,20])
    if color == 'topology':
        return (x_values, y_values, label, [COLORS[topology]],#colordict
                MARKER_DICT[f'{topology}'])
    else:
        return (x_values, y_values, label, [colordict[transporters]], MARKER_DICT[f'{topology}'])


def determine_Nlabel(seats, transporters):
    if seats == 1:
        car = 'Taxis'
        if transporters == 1:
            car = 'Taxi'
    else:
        car = 'Busses'
        if transporters == 1:
            car = 'Bus'
    return f'{transporters} {car}'


def determine_legend(topology, compare='topologies'):
    '''
    create label from topology
    '''
    if compare == 'topologies':
        name = topology.split('_')
        if len(name) == 2:
            label = f'{name[0].capitalize()}, {name[1]} Nodes'
        elif name[0] == 'euclidian':
            label = 'Euclidean Plane'
        elif name[0] == 'triangular':
            label = 'Triangular Lattice'
        elif name[0] == 'hexagonal':
            label = 'Hexagonal Lattice'
        elif name[0] == 'manhattan':
            label = 'Manhattan Network'
        elif len(name) > 2:
            if name[0] == 'star' and name[1] == 'weighted':
                label = f'Wheel Graphs, $w = {name[2]}$'
        else:
            label = f'{name}'
        return label


def check_lenghts(ratelist, y_values, topology):
    if len(ratelist) != len(y_values):
        raise AssertionError(f'Lengths not Matching for {topology}')


def ontheway_plot(dataDict, metaDict, save_dir):
    specifications = {}
    specifications.update({'parameter': 'rejection_ratio', 'xaxis': 'Rejection Ratio', 'xrange': [0.0, 1.0],
                           'observe': 'on_the_way_ness', 'yaxis': 'On-the-way-ness', 'yrange': None,
                           'topology': 'toy_8',
                           'seats': [8], 'transporters': [10],
                           'ratelist': [5.0],
                           'invehicle_t': DEFAULTINVEHICLE, 'total_t': DEFAULTTOTAL,
                           'marker': MARKER_DICT, 'color': COLORS,
                           'title': 'Comparison of different Topologies'})
    list_of_xy_pairs = prepare_x_y(dataDict, specifications, metaDict)
    plot(specifications, save_dir, list_of_xy_pairs)



def coarse_grain_or_wheel_plot(dataDict, observe, save_dir, transporterlist, topology_list, name):
    '''
    plot 3 grid sizes and euclidian for certain number of transporters
    :param topology_list:
    :param dataDict:
    :param observe:
    :param parameter:
    :param RATELIST:
    :param colors:
    :param transporterlist:
    :return:
    '''
    for transporters in transporterlist:
        plot_vectors = []
        for topology in topology_list:
            plot_vector = prepare_standard_x_y(topology, transporters, dataDict, observe)
            plot_vectors.append(plot_vector)
        specifications = {'parameter': 'rate', 'observe': observe,
                          'transporters': [transporters], 'xaxis': determine_x_axis('rate')[0],
                          'xrange': determine_x_axis('rate')[1], 'yaxis': determine_y_axis(observe)[0],
                          'yrange': determine_y_axis(observe)[1]}

        specifications['topology'] = f'{name}_{transporters}'
        if name == 'coarse_grain':
            specifications['title'] = f'Coarse Graining N={transporters}'
        elif name == 'wheel':
            specifications['title'] = f'Wheel N={transporters}'
        plot(specifications, save_dir, plot_vectors)

def errorbars_plot(dataDict, metaDict, observe, save_dir):
    for topology in ['geometric']: #'manhattan_data_2011_1',
        specifications = metaDict[f'{topology}_2_rate_{observe}'].copy()
        if topology == 'manhattan_data_2011_1':
            sim_list = [1, 2, 3]
        elif topology == 'geometric':
            sim_list = [2, 3, 4]
        all_y_values = []#simulation,transporters,rate
        specifications['xrange'] = [0, 8.1]
        specifications['ratelist'] = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 8.0]
        for sim_number in sim_list:
            specifications['topology'] = f'combine-{topology}_{sim_number}'
            specifications['transporters'] = [1, 10, 20]
            plot_vector = prepare_x_y(dataDict, specifications, metaDict)
            y_values_sim = []
            for N in plot_vector:
                y_values_sim.append(N[1]) #list of lists
            all_y_values.append(y_values_sim)

        y_values_average = []
        errorbars = []
        for i in range(len([1,10,20])):
            y_values_average_N = []
            errorbars_N = []
            for index in range(len([0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 8.0])):
                to_be_averaged = []
                for j in range(len(sim_list)):
                    to_be_averaged.append(all_y_values[j][i][index])
                avg = average(to_be_averaged)
                errorbars_N.append(error(to_be_averaged, avg))
                y_values_average_N.append(avg)
            y_values_average.append(y_values_average_N)
            errorbars.append(errorbars_N)
        for i in range(3):
            plot_vector[i][1] = y_values_average[i]
            plot_vector[i].append(errorbars[i])
        plot(specifications, save_dir, plot_vector)

def average(list):
    sum = 0.0
    for value in list:
        sum += value
    return sum/len(list)

def error(list, average):
    sum = 0.0
    for value in list:
        sum += (value - average)**2
    return np.sqrt(sum/(len(list)-1))

def first_taxi_plot(dataDict, metaDict, observe, save_dir):
    specifications = metaDict['grid_100_rate_rejection_ratio'].copy()
    specifications['observe'] = observe

    specifications['transporters'] = [10]
    specifications['seats'] = [1]
    specifications['xaxis'], specifications['xrange'] = determine_x_axis('rate')
    specifications['yaxis'], specifications['yrange'] = determine_y_axis(observe)
    specifications['title'] = f'10 Taxis on 10 by 10 Grid'
    plot_vector = prepare_x_y(dataDict, specifications, metaDict)
    specifications['topology'] = f'first_taxi' #just for savename
    plot(specifications, save_dir, plot_vector)

def compare_topologies(dataDict, specifications, metaDict):
    '''
    compare grid to x
    same as standard plotting, just add type of topology to label
    :param dataDict:
    :param topology:
    :param observe:
    :param parameter:
    :param RATELIST:
    :param seatlist:
    :param colors:
    :param markerdict:
    :param transporterlist:
    :return:list_of_xy_pairs
    '''
    parameter = specifications['parameter']
    observe = specifications['observe']
    topology = specifications['topology']
    seatlist = specifications['seats']
    transporterlist = specifications['transporters']
    RATELIST = specifications['ratelist']
    markerdict = MARKER_DICT
    colors = specifications['color']
    list_of_xy_pairs = []
    if topology.split('-')[0] == 'combine':
        assert parameter == 'rate'
        seats = 8
        for topo in [topology.split("-")[1], 'grid_100']:
            #if topo == 'grid_100':
                #specifications = metaDict[f'grid_100_rate_{observe}']
            for transporters in transporterlist:
                label = f'N={transporters} {topo.split("_")[0].capitalize()}'
                try:
                    plotdict = dataDict[
                        f'{topo}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                except KeyError:
                    print(f'{topo}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL} not found')
                    break

                if RATELIST == None:
                    ratelist = plotdict['rate'].copy()
                else:
                    ratelist = RATELIST

                x_values, y_values = select_x_y(plotdict, ratelist, observe)

                check_lenghts(x_values, y_values, topo)
                list_of_xy_pairs.append([x_values, y_values, label, [colors[transporters]],
                                         MARKER_DICT[f'{topo}']])

    # on the way ness plots
    elif observe == 'on_the_way_ness':
        for network in ['grid', 'star', 'chain']:
            for size in [25, 100, 400]:
                topology = f'{network}_{size}'
                label = determine_legend(topology)
                try:
                    for seats in seatlist:
                        for transporters in [10]:
                            plotdict = dataDict[f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                            if RATELIST == None:
                                ratelist = plotdict['rate'].copy()
                            else:
                                ratelist = RATELIST.copy()
                            indices = [plotdict['rate'].index(rate) for rate in ratelist]

                            if observe == 'on_the_way_ness' and parameter == 'rejection_ratio':
                                with open(f'{RESULT_DATA_ROOT_DIRECTORY}/ontheway.json', 'r') as infile:
                                    ontheway = json.load(infile)
                                onthewayness = ontheway[f'{topology}']
                                list_of_xy_pairs.append(
                                    [[plotdict[parameter][index] for index in indices], [onthewayness], label,
                                     [colors[f'{network}_100']], markerdict[str(size)]])

                except KeyError:
                    print(
                        f'No Data for topology comparison {topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}')


    # Compare how similar Taxis are
    elif topology == 'toy_8':
        for network in ['grid', 'star', 'chain']:
            for size in [100]:
                topology = f'{network}_{size}'
                label = determine_legend(topology)
                for seats in seatlist:
                    for transporters in [10]:
                        try:
                            plotdict = dataDict[
                                f'{topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}']
                            if RATELIST == None:
                                ratelist = plotdict['rate']
                            indices = [plotdict['rate'].index(rate) for rate in ratelist]
                            x_values = [plotdict[parameter][index] for index in indices]
                            y_values = [plotdict[observe][index] for index in indices]
                            list_of_xy_pairs.append([x_values, y_values, label,
                                                     [colors[f'{network}_100']], markerdict[str(size)]])
                            print(f'seats{seats}_transporters{transporters}_topology{topology} done')
                        except KeyError:
                            print(
                                f'No Data topology comparison {topology}_{transporters}_{seats}_{DEFAULTINVEHICLE}_{DEFAULTTOTAL}')
    else:
        raise AssertionError('function should not be called if none of the if statements are met')
    return list_of_xy_pairs

def select_x_y(plotdict, ratelist, observe):
    indices = []
    ratelist_copy = ratelist.copy()
    ratelist_copy.sort()  # prevent rogue lines
    not_existing = []
    for rate in ratelist_copy:
        all_measured_rates = plotdict['rate']
        try:
            rate_index = all_measured_rates.index(rate)
            indices.append(rate_index)
        except ValueError:
            not_existing.append(rate)  # plot data that exists # ACTUALLY REMOVES RATE ALSO FROM list_of_xy_pairs, where was appended earlier!!!!!!!!!!!!
            print(f'{rate} is not in the list')
    for rate in not_existing:
        ratelist_copy.remove(rate)
    x_values = ratelist_copy.copy()
    y_values = [plotdict[observe][index] for index in indices]
    return x_values, y_values

def plot(specifications, save_dir, list_of_xy_pairs):
    """
    make one plot of multiple curves with specifications dict
    :param specifications: meta data for plot
    :param save_dir: path where to save plots
    :param list_of_xy_pairs: list_of_xy_pairs
    :return:
    """
    plt.tight_layout()
    if SAVEMODE == 'tikz':
        mpl.use('pgf')
    # plt.gcf().subplots_adjust(bottom=0.3)
    fig, ax = plt.subplots()

    parameter = specifications['parameter']
    observe = specifications['observe']
    topology = specifications['topology']
    transporterlist = specifications['transporters']
    xaxis = specifications['xaxis']
    yaxis = specifications['yaxis']
    title = specifications['title']
    yrange = specifications['yrange']
    xrange = specifications['xrange']
    if parameter == 'ntaxis':
        ax.axhline(y=0.03, xmin=0, xmax=8, label='Rejection Ratio = 3\%')
        ax.axhline(y=0.11, xmin=0, xmax=8, label='Rejection Ratio = 11\%')
    elif observe == 'on_the_way_ness':
        plt.plot([0.009702, 0.03327062, 0.6015447], [0.404513888, 0.182291666,  0.1234027], color='k', linestyle='-', linewidth=2)
        plt.plot([0.024843486, 0.1814878, 0.7017474], [ 0.35027038, 0.077543, 0.030203], color='k', linestyle='-', linewidth=2)
    # try:
    for i in range(len(list_of_xy_pairs)):
        # ax.set_xticks(ratelist)
        if (len(list_of_xy_pairs[i][0]) != len(list_of_xy_pairs[i][1])):  # compare length of x and y list
            raise AssertionError('x unequal y')
        if len(list_of_xy_pairs[i])>5:
            plt.errorbar(list_of_xy_pairs[i][0], list_of_xy_pairs[i][1], list_of_xy_pairs[i][5], linestyle='--',
                         label=list_of_xy_pairs[i][2],c=(list_of_xy_pairs[i][3][0]), #marker=list_of_xy_pairs[i][4],
                     fillstyle='none', linewidth=1
                         )#, marker='s', mfc='red', mec='green', ms=20, mew=4)
            a = tikzplotlib.get_tikz_code()

        else:
            plt.plot(list_of_xy_pairs[i][0],
                     list_of_xy_pairs[i][1],
                     #yerr = errorbars,
                     linestyle='--',
                     label=list_of_xy_pairs[i][2],
                     c=(list_of_xy_pairs[i][3][0]),  # vmin=0, vmax=4,
                     marker=list_of_xy_pairs[i][4],
                     fillstyle='none',
                     linewidth=1)
    # plt.plot for --
    #        else:
    # plt.scatter(list_of_xy_pairs[i][0],
    #             list_of_xy_pairs[i][1],
    #             label=list_of_xy_pairs[i][2],
    #             c=list_of_xy_pairs[i][3], vmin=0, vmax=4,
    #             marker=list_of_xy_pairs[i][4])
    # ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    plt.xlabel(xaxis)
    plt.ylabel(yaxis)
    ax.set_ylim(yrange)
    ax.set_xlim(xrange)

    b = tikzplotlib.get_tikz_code()
    legend1 = ax.legend(fancybox=True, framealpha=0.5, frameon=False)  # , bbox_to_anchor=(0.1, 0., 0.5, 0.5))
    #debugging library

    #figure = plt.gcf()
    for child in ax.get_children():
        if isinstance(child, mpl.legend.Legend):
            texts = []
            for text in child.texts:
                texts.append("{}".format(text.get_text()))
            #data = legend.draw_legend(data, child)
    #legend1 = ax.legend(fancybox=True, framealpha=0.5, frameon=False)  # , bbox_to_anchor=(0.1, 0., 0.5, 0.5))
    c = tikzplotlib.get_tikz_code()
#    legend1.texts[3] = Text(0, 0, 'N=1 test')
    # plt.legend()
    plt.title(title)

    THESISLIST = ['all_on_the_way_ness_rejection', 'chain_100_occupancy_rate', 'chain-100_rejection_ratio',
                  'combine-chain_100_occupancy_rate', 'combine-chain_100_rejection_ratio_rate',
                  'combine-chain_100_public_good_rate',
                  'combine-euclidian_occupancy_rate', 'combine-euclidian_rejection_ratio_rate',
                  'combine-star_100_occupancy_rate', 'combine-star_100_rejection_ratio_rate',
                  'grid_100_occupancy_rate', 'grid_100_occupancy_only_rate', 'grid_100_rejection_ratio_rate',
                  'grid_100_rejection_ratio_only_rate',
                  'grid_100_public_good_rate', 'grid_100_public_good_only_rate', 'grid_100_travel_time_rate',
                  'grid_100_travel_time_only_rate',
                  'grid_100_absolute_waiting_time_rate', 'grid_100_absolute_waiting_time_only_rate', 'grid_100_rejection_ratio_ntaxis',
                  'first_taxi_absolute_waiting_time_rate', 'first_taxi_public_good_rate', 'first_taxi_rejection_ratio_rate', 'first_taxi_travel_time_rate', 'first_taxi_occupancy_rate',
                  'grid_100_occupancy_unnormed_rate',
                  'grid_100_rejection_ratio_unnormed_rate',
                  'euclidian_rejection_ratio_rate',
                  'coarse_grain_1_rejection_ratio_rate', 'coarse_grain_10_rejection_ratio_rate', 'coarse_grain_20_rejection_ratio_rate',
                  'geometric_2_rejection_ratio_rate', 'combine-triangular_rejection_ratio_rate', 'combine-hexagonal_rejection_ratio_rate',
                  'grid_t_invehicle_public_good_invehicle_t', 'grid_t_totaldelay_public_good_totaldelay',
                  'grid_t_invehicle_rejection_ratio_invehicle_t', 'grid_t_totaldelay_rejection_ratio_totaldelay',
                  'grid_t_invehicle_occupancy_invehicle_t', 'grid_t_totaldelay_occupancy_totaldelay',
                  'star_weighted_0.5_rejection_ratio_rate', 'star_weighted_0.04_rejection_ratio_rate', 'star_weighted_0.1_indexbug_rejection_ratio_rate','star_weighted_0.2_rejection_ratio_rate',
                  'toy_1_rejection_ratio_rate',
                  'grid_100_absolute_waiting_time_unnormed_rate', 'grid_100_rejection_ratio_unnormed_rate', 'grid_100_occupancy_unnormed_rate', 'grid_100_public_good_unnormed_rate',
                  'combine-geometric_4_rejection_ratio_rate', 'combine-manhattan_data_2011_1_3_rejection_ratio_rate', 'combine-manhattan_uniform_2_rejection_ratio_rate',
                  'wheel_1_rejection_ratio_rate', 'wheel_10_rejection_ratio_rate', 'wheel_20_rejection_ratio_rate',
                  'toy_8_on_the_way_ness_rejection_ratio_10.tikz',
                  ]

    if topology == 'toy_8':
        savename = f'{topology}_{observe}_{parameter}_{transporterlist[0]}'

    #    elif observe == 'absolute_accepted':
    #        savename = os.path.join('absolute_accepted', f'{topology}_{observe}_{parameter}')
    #    elif observe == 'absolute_rejected':
    #        savename = os.path.join('absolute_rejected', f'{topology}_{observe}_{parameter}')
    #    elif observe == 'absolute_waiting_time':
    #        savename = os.path.join('absolute_waiting_time', f'{topology}_{observe}_{parameter}')
    #    elif observe == 'relative_waiting_time':
    #        savename = os.path.join('relative_waiting_time', f'{topology}_{observe}_{parameter}')
    else:
        if f'{topology}_{observe}_{parameter}' in THESISLIST:
            savename = os.path.join('thesis', f'{topology}_{observe}_{parameter}')
        else:
            savename = os.path.join(observe, f'{topology}_{observe}_{parameter}')
            if topology.split('-')[0] == 'combine':
                savename = os.path.join('combinations', savename)

    if SAVEMODE == 'pdf':
        plt.savefig(os.path.join(save_dir, f'{savename}.pdf'))
    elif SAVEMODE == 'tikz':
        #tikz_save(os.path.join(save_dir, f'{savename}.tikz'))
        #fig.savefig(os.path.join(save_dir, f'{savename}.pgf'))
        fig.savefig(os.path.join(save_dir, f'{savename}.tikz'), format='pgf')
    else:
        raise AssertionError('No savemode selected')

    plt.close()
    # except KeyError:
    #    print(f'{observe} not observed for {topology}')
    return 0


def createdirectories(savedir):
    try:
        os.makedirs(savedir)  # can create subdirectories
    except OSError:
        print(f'{savedir} already exists')
    else:
        print(f'Successfully created the directory {savedir}')

    try:
        os.makedirs(f'{savedir}/correlation')  # can create subdirectories
    except OSError:
        print(f'{savedir}/correlation already exists')
    try:
        os.makedirs(f'{savedir}/geometric')  # can create subdirectories
    except OSError:
        print(f'{savedir}/geometric already exists')
    for observable in OBSERVELIST:
        try:
            os.makedirs(os.path.join(savedir, observable))  # can create subdirectories
        except OSError:
            print(f'{savedir}/{observable} already exists')
    for observable in OBSERVELIST:
        try:
            os.makedirs(os.path.join(savedir, 'combinations', observable))  # can create subdirectories
        except OSError:
            print(f'{savedir}/combinations/{observable} already exists')
    try:
        os.makedirs(os.path.join(savedir, 'thesis'))
    except OSError:
        print(f'{savedir}/thesis already exists')


#    try:
##        os.makedirs(f'{savedir}/absolute_rejected')  # can create subdirectories
#   except OSError:
#       print(f'{savedir}/absolute_rejected already exists')
#   try:
#       os.makedirs(f'{savedir}/relative_waiting_time')  # can create subdirectories
#  except OSError:
#      print(f'{savedir}/relative_waiting_time already exists')
#  try:
##      os.makedirs(f'{savedir}/absolute_waiting_time')  # can create subdirectories
# except OSError:
#        print(f'{savedir}/absolute_waiting_time already exists')
if __name__ == "__main__":
    main()
