for i in 1,8,0.5 1,8,1.0 1,8,1.5 1,8,2.0 1,8,2.5 1,8,3.0 1,8,4.0 1,8,5.0 1,8,6.0 1,8,8.0 10,8,0.5 10,8,1.0 10,8,1.5 10,8,2.0 10,8,2.5 10,8,3.0 10,8,4.0 10,8,5.0 10,8,6.0 10,8,8.0 20,8,0.5 20,8,1.0 20,8,1.5 20,8,2.0 20,8,2.5 20,8,3.0 20,8,4.0 20,8,5.0 20,8,6.0 20,8,8.0;
do IFS=",";
	set -- $i;
	echo "Seats: $2 Number of Transporters: $1 Rho: $3";
	sbatch -n 1 -N 1 --mem=5G --open-mode=append -t 2-00:00:00 -o 'redooutput.txt' --wrap="~/.conda/envs/d3t_3/bin/python cluster_simulation_grid.py $3 'geometric_4' $1 $2 'geometric_100'";
done;
